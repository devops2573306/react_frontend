import React, {useState, useEffect} from "react";
import {BrowserRouter, BrowserRouter as Router, Route, Routes} from "react-router-dom";
import './App.css';
import Navbar from './Components/Navbar';
import Home from './Pages/Home';
import CreatePlayer from "./Pages/CreatePlayer";
import Players from "./Pages/Players";
import PlayerDetails from "./Pages/PlayerDetails";
//import EditPlayer from "./Components/EditPlayer";
//import About from "./Pages/About";
//import Players from "./Pages/Players";

export default function App() {
    return (
        <div>
            <div className={"navbar"}>
                <Navbar />
            </div>
            <BrowserRouter>
                <Routes>
                    <Route index element={<Home/>} />
                    <Route path={"/home"} element={<Home/>} />
                    <Route path={"/create"} element={<CreatePlayer />} />
                    <Route path={"/players"} element={<Players />} />
                    <Route path={"/players/details"} element={<PlayerDetails />} />
                </Routes>
            </BrowserRouter>
        </div>
    )
}
