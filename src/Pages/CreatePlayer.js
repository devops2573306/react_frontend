import React, {useState} from "react";
import axios from "axios";

export default function CreatePlayer() {
            const [firstName, setFirstName] = useState('');
            const [lastName, setLastName] = useState('');
            const [team, setTeam] = useState('');
            const [nationality, setNationality] = useState('');
            const [age, setAge] = useState('');
            const [shirtNumber, setShirtNumber] = useState('');

            function submitPlayer() {
                const player = {
                    firstName: firstName,
                    lastName: lastName,
                    team: team,
                    nationality: nationality,
                    age: age,
                    shirtNumber: shirtNumber
                }

                axios.post('http://localhost:8080/api/v1/players', player)
                    .then(res => {
                        console.log("Player submitted successfully", res.data);
                    })
                    .catch(error => {
                        console.log("Error submitting player", error);
                    })
            }

            const handleSubmit = (e) => {
                e.preventDefault();
                submitPlayer();
            }

            return (
                <div className={"App"}>
                    <h1>Create a Player</h1>

                    <div className={"playerForm"}>
                        <label>First Name</label>
                        <input type={"text"} name={"firstName"} id={"firstName"} onChange={(e) =>
                            setFirstName(e.target.value)}/>

                        <label>Last Name</label>
                        <input type={"text"} name={"lastName"} id={"lastName"} onChange={(e) =>
                            setLastName(e.target.value)}/>

                        <label>Team</label>
                        <input type={"text"} name={"team"} id={"team"} onChange={(e) =>
                            setTeam(e.target.value)}/>

                        <label>Nationality</label>
                        <input type={"text"} name={"nationality"} id={"nationality"} onChange={(e) =>
                            setNationality(e.target.value)}/>

                        <label>Age</label>
                        <input type={"number"} name={"age"} id={"age"} onChange={(e) =>
                            setAge(e.target.value)}/>

                        <label>Shirt Number</label>
                        <input type={"number"} name={"shirtNumber"} id={"shirtNumber"} onChange={(e) =>
                            setShirtNumber(e.target.value)}/>

                        <button onClick={submitPlayer} id={"submitButton"}>Submit</button>
                    </div>
                </div>
    )
}
