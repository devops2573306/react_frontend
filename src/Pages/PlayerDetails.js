import React, {useEffect, useState} from "react";
import axios from "axios";

export default function PlayerDetails(id) {
    const [playerData, setPlayerData] = useState([]);
    const [isVisible, setIsVisible] = useState(false);
    const [editPlayer, setEditPlayer] = useState(null);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [team, setTeam] = useState('');
    const [nationality, setNationality] = useState('');
    const [age, setAge] = useState('');
    const [shirtNumber, setShirtNumber] = useState('');

    // function editPlayer() {
    //     const player = {
    //         firstName: firstName,
    //         lastName: lastName,
    //         team: team,
    //         nationality: nationality,
    //         age: age,
    //         shirtNumber: shirtNumber
    //     }}

    const toggleVisibility = () => {
        setIsVisible(!isVisible);
    };

    useEffect(() => {
        axios.get(`http://localhost:8080/api/v1/players/ordered`)
            .then((response) => {
                setPlayerData(response.data);
            })
            .catch ((error) => {
                console.log("Error fetching data: ", error);
            });
    }, []);

    const deletePlayer = (id) => {
        axios.delete(`http://localhost:8080/api/v1/players?id={id}`, id)
            .then((response) => {
                console.log(response.data);
            })
            .catch((error) => {
                console.log(error);
        })
    }

    return (
        <div className={"playerDetails"}>
            <h1>Player Details</h1>
            <p></p>
            {playerData.map((player) => (
                <div key={player.id} className={"playerContainer"}>
                    <p><b>Name</b>: {player.firstName + " " + player.lastName}</p>
                    <p><b>Age</b>: {player.age}</p>
                    <p><b>Country</b>: {player.nationality}</p>
                    <p><b>Team</b>: {player.team}</p>
                    <p><b>Number</b>: {player.shirtNumber}</p>
                    <button onClick={toggleVisibility} id={"editButton"}>Edit</button>
                    <button onClick={() => {deletePlayer(id)}} id={"deleteButton"}>Delete</button>
                    <br></br>

                    {isVisible && (
                        <div className={"editForm"}>
                            <label>Name </label>
                            <input type={"text"} name={"firstName"} id={"firstName"} onChange={(e) =>
                                setFirstName(e.target.value)}/>

                            <label>Last Name</label>
                            <input type={"text"} name={"lastName"} id={"lastName"} onChange={(e) =>
                                setLastName(e.target.value)}/>

                            <label>Team</label>
                            <input type={"text"} name={"team"} id={"team"} onChange={(e) =>
                                setTeam(e.target.value)}/>

                            <label>Nationality</label>
                            <input type={"text"} name={"nationality"} id={"nationality"} onChange={(e) =>
                                setNationality(e.target.value)}/>

                            <label>Age</label>
                            <input type={"number"} name={"age"} id={"age"} onChange={(e) =>
                                setAge(e.target.value)}/>

                            <label>Shirt Number</label>
                            <input type={"number"} name={"shirtNumber"} id={"shirtNumber"} onChange={(e) =>
                                setShirtNumber(e.target.value)}/>

                            <button id={"updateButton"}>Update</button>
                        </div>
                    )}
                </div>
            ))}
        </div>
    )
}