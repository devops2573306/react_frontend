import React, { useState } from "react";
import axios from "axios";

export default function EditPlayer({id}) {
            axios.put("https://localhost:8080/api/v1/players/${id}")
                .then((response) => {
                    return response.data;
                })
                .catch((error) => {
                    console.log("Error updating user", error);
                })
}

