import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import axios from "axios";

export default function Players() {
    let navigate = useNavigate();
    const redirectPage = () => {
        let path = "/players/details"
        navigate(path);
    }

    const [playerData, setPlayerData] = useState([]);

    useEffect(() => {
        axios.get("http://localhost:8080/api/v1/players/ordered")
            .then((response) => {
                console.log(response.data);
                setPlayerData(response.data);
            })
            .catch ((error) => {
                console.error("Error fetching api: ", error);
            });
    }, []);

    return (
        <div className={"playerData"}>
            <h1>Players</h1>
            <button id={"detailsButton"} onClick={redirectPage}>Details</button>
            {playerData.map((player) => (

                <div key={player.id} className={"playerList"}>
                    <p>{player.firstName + " " + player.lastName}</p>
                </div>
            ))}
        </div>
    )
}
