export default function Navbar() {
    return (
        <div className={"links"}>
            <a href={"/home"}>Home</a>
            <a href={"/create"}>Create Players</a>
            <a href={"/players"}>Players</a>
            <a href={"/about"}>About</a>
        </div>
    )
}